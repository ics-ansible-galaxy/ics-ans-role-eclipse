import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')

VERSION = '4.7.3a'


def test_eclipse_installed(host):
    eclipse_bin = host.file('/opt/eclipse-{}/eclipse'.format(VERSION))
    assert eclipse_bin.is_file
    assert eclipse_bin.mode == 0o755
