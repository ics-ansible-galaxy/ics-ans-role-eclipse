ics-ans-role-eclipse
====================

Ansible role to install Eclipse for RCP and RAP Developers.

Requirements
------------

- ansible >= 2.7
- molecule >= 2.6

Role Variables
--------------

```yaml
eclipse_version: 4.7.3a
eclipse_archive: https://artifactory.esss.lu.se/artifactory/swi-pkg/eclipse/{{ eclipse_version }}/eclipse-rcp-oxygen-3a-linux-gtk-x86_64.tar.gz
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-eclipse
```

License
-------

BSD 2-clause
